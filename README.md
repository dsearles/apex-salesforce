# Salesforce Apex

Test project with:

* **Language:** Apex
* **Package Manager:** SFDX
* **Framework:** SFDX

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :white_check_mark: |
| Dependency Scanning | :x:                |
| Container Scanning  | :x:                |
| DAST                | :x:                |
| License Management  | :x:                |
